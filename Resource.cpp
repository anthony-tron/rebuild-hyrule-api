#include "Resource.h"

unsigned Resource::value() const
{
    return m_value;
}

unsigned Resource::toRupee(unsigned count) const
{
    return count * m_value;
}

Resource::Resource(unsigned value)
    : m_value(value)
{}
