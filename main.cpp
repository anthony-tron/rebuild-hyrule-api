#include <iostream>
#include "WoodCutterHouse.hpp"

int main()
{
    WoodCutterHouse house;

    std::cout << house.stock() << std::endl
              << house.retrieve() << std::endl;

    std::cout << WoodCutterHouse::resource_t().toRupee(house.stock()) << std::endl;

    return 0;
}
