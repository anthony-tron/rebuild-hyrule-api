#ifndef BUILD_H
#define BUILD_H

#include <type_traits>
#include "Resource.h"

template<typename T>
class Build
{
    static_assert (std::is_base_of<Resource, T>::value, "T must inherit from Resource");

    public:
    typedef T resource_t;
    virtual unsigned stock() const=0;
    virtual unsigned maxStock() const=0;
    virtual unsigned experience() const=0;
    virtual unsigned retrieve()=0;
};

#endif // BUILD_H
