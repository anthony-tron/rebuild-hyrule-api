#ifndef WOODCUTTERHOUSE_HPP
#define WOODCUTTERHOUSE_HPP

#include "Wood.h"
#include "Build.hpp"

class WoodCutterHouse : public Build<Wood>
{

    // Build interface
    public:
    virtual unsigned stock() const override;
    virtual unsigned maxStock() const override;
    virtual unsigned experience() const override;
    virtual unsigned retrieve() override;
};

inline unsigned WoodCutterHouse::stock() const
{
    return 2;
}

inline unsigned WoodCutterHouse::maxStock() const
{
    return 3;
}

inline unsigned WoodCutterHouse::experience() const
{
    return 4;
}

inline unsigned WoodCutterHouse::retrieve()
{
    return 5;
}

#endif // WOODCUTTERHOUSE_HPP
