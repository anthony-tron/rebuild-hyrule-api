#ifndef RESOURCE_H
#define RESOURCE_H


class Resource
{
    public:
    Resource(unsigned value);
    unsigned value() const;
    unsigned toRupee(unsigned count = 0) const;

    private:
    unsigned m_value;
};

#endif // RESOURCE_H
